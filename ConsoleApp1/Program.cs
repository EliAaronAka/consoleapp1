﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class PropertyClass
    {
        public int Num { get; set; } 
    }

    public class AClass 
    {
        public PropertyClass Prop { get; set; }
        public Func<string> Func { get; }
        public AClass()
        {
            Func = () =>
            {
                return Prop != null ? Prop.Num.ToString() : "No value!";
            };
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            AClass aClass = new AClass();
            int aq;
            Console.WriteLine(aClass.Func());

            aClass.Prop = new PropertyClass();
            Console.WriteLine(aClass.Func());

            aClass.Prop.Num = 23;
            Console.WriteLine(aClass.Func());

            aClass.Prop = new PropertyClass();
            Console.WriteLine(aClass.Func());

            aClass.Prop = null;
            Console.WriteLine(aClass.Func());

            Console.ReadKey();
        }
    }
}
